package net.derstefon.springWebclientShowcase;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FluxWithMonosToMonoWithBuffer
{

    private final static Logger LOGGER = LoggerFactory.getLogger(FluxWithMonosToMonoWithBuffer.class);

    public static void main(String[] args)
    {
        new FluxWithMonosToMonoWithBuffer().start();
    }

    private void start()
    {
        Flux<String> elementsFlux = getGroup().flatMapIterable(Group::getElements);
        Flux<MappedElement> mappedElementsFlux = elementsFlux.flatMap(this::getMappedElement);
        List<MappedElement> mappedElements = mappedElementsFlux.buffer().blockLast();
        mappedElements.forEach(this::print);
    }

    private void print(final MappedElement mappedElement)
    {
        LOGGER.info(mappedElement.toString());
    }

    private Mono<Group> getGroup()
    {
        LOGGER.info("getGroup");
        return Mono.just(new Group(Arrays.asList("7", "1", "2", "3"))).delayElement(Duration.ofSeconds(3));
    }

    private Mono<MappedElement> getMappedElement(String element)
    {
        return Mono.just(new MappedElement(element + "!")).delayElement(Duration.ofSeconds(Long.parseLong(element)));
    }

    private static class Group
    {
        private final Collection<String> elements;

        public Group(final Collection<String> elements)
        {
            this.elements = elements;
        }

        public Collection<String> getElements()
        {
            return elements;
        }
    }

    private static class MappedElement
    {
        public String value;

        public MappedElement(final String value)
        {
            this.value = value;
        }

        @Override
        public String toString()
        {
            return "MappedElement{" +
                "value='" + value + '\'' +
                '}';
        }
    }
}
