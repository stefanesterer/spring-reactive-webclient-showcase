package net.derstefon.springWebclientShowcase;

import reactor.core.publisher.Mono;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.reactive.function.client.WebClient;

public class SpringWebClientClient
{

    private final static Logger LOGGER = LoggerFactory.getLogger(SpringWebClientClient.class);

    public static void main(String[] args)
    {
        LOGGER.info("===== START ====== ");

        Mono<String> fooMono = WebClient.create()
            .get()
            .uri("localhost:8080/foo")
            .retrieve()
            .bodyToMono(String.class);

        LOGGER.info("===== FOO MONO CREATED ======");

        Mono<String> barMono = WebClient.create()
            .get()
            .uri("localhost:8080/bar")
            .retrieve()
            .bodyToMono(String.class);

        LOGGER.info("===== BAR MONO CREATED ======");

        Mono<String> combined = Mono.zip(fooMono, barMono, (foo, bar) ->
            foo + bar
        );

        combined.subscribe(SpringWebClientClient::doit);
        combined.block();
        LOGGER.info("===== BLOCK DONE ======");

        LOGGER.info("===== STOP ======");
    }

    private static void doit(String value)
    {
        LOGGER.info(String.format("===== COMBINED %s ======", value));
    }

}
