package net.derstefon.springWebclientShowcase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWebclientShowcaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringWebclientShowcaseApplication.class, args);
	}

}
