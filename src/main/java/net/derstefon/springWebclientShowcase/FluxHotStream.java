package net.derstefon.springWebclientShowcase;

import static java.time.Duration.ofSeconds;

import reactor.core.publisher.ConnectableFlux;
import reactor.core.publisher.Flux;

public class FluxHotStream
{

    public static void main(String[] args)
    {
        ConnectableFlux<Object> publish = Flux.create(fluxSink -> {
            while (true)
            {
                fluxSink.next(System.currentTimeMillis());
            }
        })
            .sample(ofSeconds(2))
            .publish();

        publish.subscribe(System.out::println);
        publish.subscribe(time -> {
            System.out.println("Second: " + time);
        });

        publish.connect();
    }

}
