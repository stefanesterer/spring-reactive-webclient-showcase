package net.derstefon.springWebclientShowcase.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FooBarController
{

    @GetMapping("/foo")
    public String foo() throws InterruptedException
    {
        Thread.sleep(10000L);
        return "foo";
    }

    @GetMapping("/bar")
    public String bar() throws InterruptedException
    {
        Thread.sleep(10000L);
        return "bar";
    }

}
