package net.derstefon.springWebclientShowcase;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import org.springframework.util.StringUtils;


// https://www.baeldung.com/reactor-core
public class MonoVsFlux
{


    public static void main(String[] args)
    {
        // 0 .. n elements
        Flux<String> flux = Flux.just("a", "b", "c");

        flux.map(StringUtils::capitalize)
            .subscribe(System.out::println);

        // 0 .. 1 elemnt
        Mono<String> mono = Mono.just("foo");

        mono.map(StringUtils::capitalize)
            .subscribe(System.out::println);

        // The core difference is that Reactive is a push model, whereas the Java 8 Streams are a pull model. In a
        // reactive approach, events are pushed to the subscribers as they come in.

    }

}
