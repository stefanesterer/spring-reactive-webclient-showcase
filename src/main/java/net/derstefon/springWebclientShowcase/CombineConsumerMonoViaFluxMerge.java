package net.derstefon.springWebclientShowcase;

import java.time.Duration;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CombineConsumerMonoViaFluxMerge
{

    private final static Logger LOGGER = LoggerFactory.getLogger(CombineConsumerMonoViaFluxMerge.class);

    static class TestDTO
    {
        public String first;
        public String second;
        public String third;

        @Override
        public String toString()
        {
            return "TestDTO{" +
                "first='" + first + '\'' +
                ", second='" + second + '\'' +
                ", third='" + third + '\'' +
                '}';
        }
    }

    public static void main(String[] args)
    {
        Mono<Consumer<TestDTO>> foo = createMono("foo", s -> dto -> dto.first = s);
        Mono<Consumer<TestDTO>> bar = createMono("bar", s -> dto -> dto.second = s);
        Mono<Consumer<TestDTO>> baz = createMono("baz", s -> dto -> dto.third = s);

        Flux<Consumer<TestDTO>> merge = Flux.merge(foo, bar, baz);

        LOGGER.info("FLUX BLOCK START");
        List<Consumer<TestDTO>> consumers = merge.buffer().blockLast();
        LOGGER.info("FLUX BLOCK ENDS");

        TestDTO newDTO = new TestDTO();
        consumers.forEach(testDTOConsumer -> doOnNext(testDTOConsumer, newDTO));

        LOGGER.info(newDTO.toString());
    }

    private static Mono<Consumer<TestDTO>> createMono(String value, Function<String, Consumer<TestDTO>> mapper)
    {
        return Mono.just(value)
            .delayElement(Duration.ofSeconds(5))
            .map(mapper);
    }

    private static void doOnNext(final Consumer<TestDTO> stringConsumer,
        final TestDTO newDTO)
    {
        LOGGER.info(stringConsumer.toString());
        stringConsumer.accept(newDTO);
    }

}
